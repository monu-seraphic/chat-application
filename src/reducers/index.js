import archiveReducer from "./archive";
import favoritesReducer from "./favorites";
import chatsReducer from "./chat";

export {
	archiveReducer,
	favoritesReducer,
	chatsReducer
}