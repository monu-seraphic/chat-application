const chats = (state = [], action) => {
  let chats = [{
  	name: "Townsend Seary",
  	lastMessgae: "What's up, how are you?",
  	lastMessgaeType: "Text",
  	image: "/images/sample/user-basic-1.jpg",
  	lastMessgaeDate: "03:41 PM",
  	hasUnread: true,
    unreadCount: 3,
    id: 1
  }, {
  	name: "Forest Kroch",
  	lastMessgae: "",
  	lastMessgaeType: "Image",
  	image: "/images/sample/user-basic-2.jpg",
  	lastMessgaeDate: "Yesterday",
  	hasUnread: false,
    unreadCount: 1,
    id: 2
  },{
  	name: "Byrom Guittet",
  	lastMessgae: "I sent you all the files. Good luck with 😃",
  	lastMessgaeType: "Text",
  	image: "/images/sample/user-large-8.png",
  	lastMessgaeDate: "11:05 AM",
  	hasUnread: true,
    unreadCount: 0,
    id: 3
  },{
    name: "Margaretta Worvell",
    lastMessgae: "I need you today. Can you come with me?",
    lastMessgaeType: "Text",
    image: "/images/sample/user-basic-4.jpg",
    lastMessgaeDate: "03:41 PM",
    hasUnread: true,
    unreadCount: 88,
    id: 4
  },{
    name: "😍 My Family 😍",
    lastMessgae: "Hello!!!",
    lastMessgaeType: "Text",
    image: "/images/user-guest-avatar-neutral.svg",
    lastMessgaeDate: "Yesterday",
    hasUnread: false,
    unreadCount: 0,
    id: 5
  },{
    name: "Jennica Kindred",
    lastMessgae: "",
    lastMessgaeType: "Video",
    image: "/images/user-guest-avatar-neutral.svg",
    lastMessgaeDate: "Yesterday",
    hasUnread: false,
    unreadCount: 0,
    id: 6
  },{
    name: "Marvin Rohan",
    lastMessgae: "Have you prepared the files?",
    lastMessgaeType: "Text",
    image: "",
    lastMessgaeDate: "Yesterday",
    hasUnread: true,
    unreadCount: 0,
    id: 7
  },{
    name: "Townsend Seary",
    lastMessgae: "Where are you?",
    lastMessgaeType: "Text",
    image: "/images/sample/user-basic-5.jpg",
    lastMessgaeDate: "03:41 PM",
    hasUnread: true,
    unreadCount: 0,
    id: 8
  },{
    name: "Gibb Ivanchin",
    lastMessgae: "I want to visit them.",
    lastMessgaeType: "Text",
    image: "",
    lastMessgaeDate: "03:41 PM",
    hasUnread: true,
    unreadCount: 0,
    id: 9
  },{
    name: "Harald Kowalski",
    lastMessgae: "Reports are ready.",
    lastMessgaeType: "Text",
    image: "/images/sample/user-basic-6.jpg",
    lastMessgaeDate: "03:41 PM",
    hasUnread: true,
    unreadCount: 0,
    id: 10
  },{
    name: "Afton McGilvra",
    lastMessgae: "I do not know where is it. Don't ask me, please.",
    lastMessgaeType: "Text",
    image: "",
    lastMessgaeDate: "03:41 PM",
    hasUnread: false,
    unreadCount: 0,
    id: 11
  },{
    name: "Alexandr Donnelly",
    lastMessgae: "Can anyone enter the meeting?",
    lastMessgaeType: "Text",
    image: "/images/sample/user-basic-7.jpg",
    lastMessgaeDate: "03:41 PM",
    hasUnread: true,
    unreadCount: 0,
    id: 11
  }];
  return chats
}

export default chats