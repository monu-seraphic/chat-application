import { createStore } from 'redux';

import { chatsReducer } from '../reducers';

const chatStore = createStore(chatsReducer);

export default chatStore;