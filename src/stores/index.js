import archiveStore from "./archive";
import favoritesStore from "./favorites";
import chatStore from "./chats";

export {
	archiveStore,
	favoritesStore,
	chatStore
}