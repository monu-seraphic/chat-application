import { createStore } from 'redux';

import { favoritesReducer } from '../reducers';

const favoritesStore = createStore(favoritesReducer);

export default favoritesStore;





