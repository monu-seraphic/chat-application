import { createStore } from 'redux';

import { archiveReducer } from '../reducers';

const archiveStore = createStore(archiveReducer);

export default archiveStore;




