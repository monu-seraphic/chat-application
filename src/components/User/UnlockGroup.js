import React, { Component } from "react";

class UnlockGroup extends Component {

  render() {
      return (
      
        <div className="modal fade" id="modalUnlockMediaGroup" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">
                          Unlock Media
                        </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <i className="icon icon-close"></i>
                        </button>
                    </div>
        
                    <div className="modal-body scroll-container pr-0 pb-0">
                      <label for="checkAllLockedItems">
                        <input type="button" className="btn btn-link btn-link-primary pa-0" id="checkAllLockedItems" value="Check All Items" ></input>
                      </label>
                      
                      <div className="scrollable-x pb-8">
                        <ul className="list-inline flex-nowrap mr-40 ml-n-12 pl-4" id="media-locked-list">
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-1">
                              <input type="checkbox" className="message-media-check" id="image-check-1"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-2">
                              <input type="checkbox" className="message-media-check" id="image-check-2"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-3">
                              <input type="checkbox" className="message-media-check" id="image-check-3"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-4">
                              <input type="checkbox" className="message-media-check" id="image-check-4"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-5">
                              <input type="checkbox" className="message-media-check" id="image-check-5"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-6">
                              <input type="checkbox" className="message-media-check" id="image-check-6"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                          <li className="list-inline-item flex-one min-fw-160 min-fw-lg-200">
                            <label className="message-media message-media-locked" for="image-check-7">
                              <input type="checkbox" className="message-media-check" id="image-check-7"></input>
                              <div className="message-media-block">
                                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                                  <div className="message-media-lock" style={{"backgroundImage": "/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                                </div>
                              </div>
                            </label>
                          </li>
                        </ul>
                      </div>
                    </div>
        
                    <div className="modal-body">
                      <h3 className="mb-0">
                        Photo Name
                        <small className="d-block text-muted mt-4">Unlock media for 100 Stars each</small>
                      </h3>
                    </div>
        
                    <div className="modal-footer">
                      <button type="button" className="btn btn-block btn-outline-light mb-8 mt-0" data-dismiss="modal" aria-label="Close">
                        Cancel
                      </button>
                      <button type="button" className="btn btn-block btn-primary mb-8 mt-0" data-toggle="modal" data-target="#modalViewMediaSingle" data-dismiss="modal">
                        Unlock All
                      </button>
                    </div>
                </div>
            </div>
        </div>
       

        );
    
    }
  }
  
  
  export default UnlockGroup;
  