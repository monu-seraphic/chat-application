import React, { Component } from "react";

class ContactInfo extends Component {

  render() {
      return (
        <div id="contact-information" className="sidebar">
  <header className="pb-20">
    <span className="flex-one flex-xlg-auto">
      Profile
    </span>
    <ul className="list-inline">
      <li className="list-inline-item">
        <a href="#" className="btn btn-outline-light text-danger sidebar-close">
          <i className="icon icon-close"></i>
        </a>
      </li>
    </ul>
  </header>

  <div className="sidebar-body">
      <div className="px-20 px-xl-30">
          <div className="text-center">
              <figure className="avatar avatar-xl avatar-gold my-16">
                <img src="/images/sample/user-large-6.png" className="rounded-circle" alt="image" />
              </figure>

              <h5 className="mb-1">
                Mirabelle Tow
              </h5>

              <ul className="w-100 list-inline justify-content-center text-muted mw-75 mx-auto">
                <li className="list-inline-item mb-2 min-w-0">
                  <i className="icon icon-location-outline mt-1 mr-4"></i>
                  <span className="d-block text-truncate">Memphis, TN, USA</span>
                </li>
                <li className="list-inline-item mb-2">
                  <i className="icon icon-user-alt-outline mt-1 mr-4"></i>
                  <span className="d-block text-truncate">39</span>
                </li>
                <li className="list-inline-item mb-2 min-w-0">
                  <i className="icon icon-globe mt-1 mr-4"></i>
                  <a className="d-block text-truncate" href="">alberteinstein.com</a>
                </li>
              </ul>

              <ul className="w-100 list-inline justify-content-center mt-24">
                <li className="list-inline-item min-w-0">
                  <span className="badge badge-pill badge-small badge-light font-weight-normal text-truncate">
                    <i className="icon icon-star-fill text-warning"></i>
                    100
                  </span>
                </li>
                <li className="list-inline-item min-w-0">
                  <span className="badge badge-pill badge-small badge-light font-weight-normal text-truncate">
                    <i className="icon icon-user-follow"></i>
                    1,560
                  </span>
                </li>
                <li className="list-inline-item min-w-0">
                  <span className="badge badge-pill badge-small badge-light font-weight-normal text-truncate">
                    Real Estate Agent
                  </span>
                </li>
              </ul>

              <hr className="mt-24" />

              <ul className="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                  <li className="nav-item flex-one">
                      <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                          aria-controls="home" aria-selected="true">About</a>
                  </li>
                  <li className="nav-item flex-one">
                      <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                          aria-controls="profile" aria-selected="false">Media</a>
                  </li>
              </ul>
          </div>

          <div className="tab-content" id="myTabContent">
              
            <div className="tab-pane fade show active" id="contact-profile" role="tabpanel" aria-labelledby="contact-profile">
              <p className="addReadMore showlesscontent mb-30">
                  Contrary to popular belief, Lorem Ipsum is not simply random text. 
                  It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. 
                  Richard McClintock, a Latin professor at Hampd.
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum maxime quod ipsam laudantium fugiat. Aliquid quod officiis deleniti inventore libero officia? Rem labore sit placeat deleniti saepe dolorem doloribus a. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo, odit aliquam? Eius necessitatibus repellat rem neque quas ut ad, dolore consectetur natus, repudiandae impedit error temporibus, nobis vero quod molestias. Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis quisquam dolorum tempora possimus reprehenderit necessitatibus natus suscipit nesciunt beatae. Magni voluptas ab repudiandae laudantium labore doloremque voluptatem, deserunt ducimus in?
              </p>

              <ul className="w-100 list-inline mb-30">
                <li className="list-inline-item min-w-0 mb-6 ml-0 mr-6">
                  <span className="badge badge-pill badge-small badge-primary font-weight-normal text-truncate">
                    Programming
                  </span>
                </li>
                <li className="list-inline-item min-w-0 mb-6 ml-0 mr-6">
                  <span className="badge badge-pill badge-small badge-primary font-weight-normal text-truncate">
                    Communication
                  </span>
                </li>
                <li className="list-inline-item min-w-0 mb-6 ml-0 mr-6">
                  <span className="badge badge-pill badge-small badge-primary font-weight-normal text-truncate">
                    Marketing
                  </span>
                </li>
                <li className="list-inline-item min-w-0 mb-6 ml-0 mr-6">
                  <span className="badge badge-pill badge-small badge-primary font-weight-normal text-truncate">
                    Neural Networks
                  </span>
                </li>
                <li className="list-inline-item min-w-0 mb-6 ml-0 mr-6">
                  <span className="badge badge-pill badge-small badge-primary font-weight-normal text-truncate">
                    Social Media
                  </span>
                </li>
              </ul>
              
              <div className="mb-30">
                <h5 className="mb-16">Account Verification</h5>
                <ul className="list-group list-group-flush list-group-reset">
                  <li className="list-group-item media align-items-center px-0 py-8">
                    <div className="media-left">
                      <figure className="avatar">
                        <span className="avatar-title bg-facebook rounded-circle">
                          <i className="icon icon-facebook"></i>
                        </span>
                      </figure>
                    </div>
                    <div className="media-body">
                      <small className="d-block text-muted font-weight-bold text-small">Facebook</small>
                      <a href="">RHeuvell86</a>
                    </div>
                    <div className="media-right">
                      <i className="icon icon-lg icon-check-outline text-success"></i>
                    </div>
                  </li>
                  <li className="list-group-item media align-items-center px-0 py-8">
                    <div className="media-left">
                      <figure className="avatar">
                        <span className="avatar-title bg-twitter rounded-circle">
                          <i className="icon icon-twitter"></i>
                        </span>
                      </figure>
                    </div>
                    <div className="media-body">
                      <small className="d-block text-muted font-weight-bold text-small">Twitter</small>
                      <a href="">@RHeuvell86</a>
                    </div>
                    <div className="media-right">
                      <i className="icon icon-lg icon-check-outline text-success"></i>
                    </div>
                  </li>
                  <li className="list-group-item media align-items-center px-0 py-8">
                    <div className="media-left">
                      <figure className="avatar">
                        <span className="avatar-title bg-instagram rounded-circle">
                          <i className="icon icon-instagram"></i>
                        </span>
                      </figure>
                    </div>
                    <div className="media-body">
                      <small className="d-block text-muted font-weight-bold text-small">Instagram</small>
                      <a href="">InstaHeuvell</a>
                    </div>
                    <div className="media-right">
                      <i className="icon icon-lg icon-check-outline text-success"></i>
                    </div>
                  </li>
                  <li className="list-group-item media align-items-center px-0 py-8">
                    <div className="media-left">
                      <figure className="avatar">
                        <span className="avatar-title bg-linkedin rounded-circle">
                          <i className="icon icon-linkedin"></i>
                        </span>
                      </figure>
                    </div>
                    <div className="media-body">
                      <small className="d-block text-muted font-weight-bold text-small">LinkedIn</small>
                      <span>Not Verified</span>
                    </div>
                  </li>
                </ul>
              </div>

              <div className="mb-30">
                <h5 className="mb-16">
                  <i className="icon icon-md icon-instagram mr-8"></i> InstaHeuvell
                </h5>
                <div className="grid grid-keyline grid-equal-height">
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-1.jpg" alt="Insta Gallery 1" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-2.jpg" alt="Insta Gallery 2" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-3.jpg" alt="Insta Gallery 3" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-4.jpg" alt="Insta Gallery 4" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-5.jpg" alt="Insta Gallery 5" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                  <div className="grid-col-1of3">
                    <div className="grid-fill">
                      <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                        <img src="/images/sample/profile-gallery/profile-thumb-6.jpg" alt="Insta Gallery 6" className="obj-fit-cover align-absolute-middle" />
                      </figure>
                    </div>
                  </div>
                </div>
              </div>

              <div className="mb-30">
                <h5 className="mb-16">
                  Following
                </h5>
                <ul className="list-group list-group-flush list-group-reset">
                  <li className="list-group-item py-10 px-0">Deep Learning</li>
                  <li className="list-group-item py-10 px-0">Microbiology</li>
                  <li className="list-group-item py-10 px-0">History</li>
                  <li className="list-group-item py-10 px-0">Physics</li>
                  <li className="list-group-item py-10 px-0">Chemistry</li>
                </ul>
              </div>
            </div>


            <div className="tab-pane fade" id="contact-media" role="tabpanel" aria-labelledby="contact-media">
              <h5 className="mb-16">
                Uploaded Files
              </h5>
              <div className="grid grid-keyline grid-equal-height mb-30">
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-1.jpg" alt="Insta Gallery 1" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-2.jpg" alt="Insta Gallery 2" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-3.jpg" alt="Insta Gallery 3" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-4.jpg" alt="Insta Gallery 4" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-5.jpg" alt="Insta Gallery 5" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-6.jpg" alt="Insta Gallery 6" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-7.jpg" alt="Insta Gallery 7" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-8.jpg" alt="Insta Gallery 8" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-9.jpg" alt="Insta Gallery 9" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profile-thumb-10.jpg" alt="Insta Gallery 10" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
                <div className="grid-col-1of3">
                  <div className="grid-fill">
                    <figure className="aspect-ratio aspect-ratio-1by1 mb-1 p-relative">
                      <img src="/images/sample/profile-gallery/profil /e-thumb-11.jpg" alt="Insta Gallery 11" className="obj-fit-cover align-absolute-middle" />
                    </figure>
                  </div>
                </div>
              </div>
            </div>

          </div>
      </div>
  </div>
</div>
       
       );
    
  }
}


export default ContactInfo;
