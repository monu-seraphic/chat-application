import React, { Component } from "react";
import $ from "jquery";
class AddAccounts extends Component {

  render() {
      return (
        <div className="modal fade show" id="addAccount" tabindex="-1" role="dialog" style={{"display": "block;"}}>
        <div className="modal-dialog" role="document">
            <div className="modal-content min-fh-560">
                <div className="modal-header">
                    <h5 className="modal-title">
                      Add Account
                    </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <i className="icon icon-close"></i>
                    </button>
                </div>
                <div className="modal-body">
    
                  
                    
                  <form>
    
                    <fieldset className="mb-20">
                      <h5 className="mb-20">Account Details</h5>
                      
                    
                      <div className="form-group">
                        <label for="">Username</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon icon-user-alt-outline"></i>
                            </span>
                          </div>
                          <input type="text" className="form-control" placeholder="Add Username"/>
                        </div>
                      </div>
    
                   
                      <div className="form-group">
                        <label for="">Password</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon icon-password"></i>
                            </span>
                          </div>
                          <input type="password" className="form-control" placeholder="Enter Password"/>
                        </div>
                      </div>
    
                      <div className="form-group">
                        <label for="">Repeat Password</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon icon-onward"></i>
                            </span>
                          </div>
                          <input type="password" className="form-control" placeholder="Confirm Password"/>
                        </div>
                      </div>
                    </fieldset>
    
                    <fieldset className="mb-20">
                      <h5 className="mb-20">General</h5>
    
                  
                      <div className="form-group">
                        <label for="">Full Name</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon icon-user"></i>
                            </span>
                          </div>
                          <input type="text" className="form-control" placeholder="Your Name"/>
                        </div>
                      </div>
    
                    
                      <div className="form-group">
                        <label>Location</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon icon-location-outline"></i>
                            </span>
                          </div>
                          <input type="text" className="form-control" placeholder="Add Location"/>
                        </div>
                      </div>
    
                     
                      <div className="form-group">
                        <label for="">Biography</label>
                        <textarea rows="5" cols="" className="form-control" placeholder="Add short description"></textarea>
                      </div>
    
                      <div className="grid grid-narrow">
                        <div className="grid-col-xl">
                          
                         
                          <div className="form-group">
                            <label>User Type</label>
                            <div className="dropdown bootstrap-select show-tick"><select className="selectpicker show-tick" name="user-type" data-style="form-control border border-light py-10" title="Select" data-size="3" data-width="fill" tabindex="-98"><option className="bs-title-option" value=""></option>
                              <option value="user-normal">Normal</option>
                              <option value="user-pro">Pro</option>
                              <option value="user-expert">Expert</option>
                            </select><button type="button" className="btn dropdown-toggle bs-placeholder form-control border border-light py-10" data-toggle="dropdown" role="button" title="Select"><div className="filter-option"><div className="filter-option-inner"><div className="filter-option-inner-inner">Select</div></div> </div></button><div className="dropdown-menu " role="combobox"><div className="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul className="dropdown-menu inner show"></ul></div></div></div>
                          </div>
    
                        </div>
                        <div className="grid-col-xl">
    
                                                   <div className="form-group">
                            <label>Gender</label>
                            <div className="dropdown bootstrap-select show-tick"><select className="selectpicker show-tick" data-style="form-control border border-light py-10" title="Select" data-size="2" data-width="fill" tabindex="-98"><option className="bs-title-option" value=""></option>
                              <option>Male</option>
                              <option>Female</option>
                            </select><button type="button" className="btn dropdown-toggle bs-placeholder form-control border border-light py-10" data-toggle="dropdown" role="button" title="Select"><div className="filter-option"><div className="filter-option-inner"><div className="filter-option-inner-inner">Select</div></div> </div></button><div className="dropdown-menu " role="combobox"><div className="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul className="dropdown-menu inner show"></ul></div></div></div>
                          </div>
    
                        </div>
                      </div>
    
                      <div className="form-group hide" id="user-pro">
                        <label>Verification Status</label>
                        <div className="card">
                          <div className="card-block py-8 px-12">
                            <div className="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="optionsVerificationStatus1" name="optionsVerificationStatus" className="custom-control-input"/>
                              <label className="custom-control-label" for="optionsVerificationStatus1">Pending Review</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="optionsVerificationStatus2" name="optionsVerificationStatus" className="custom-control-input"/>
                              <label className="custom-control-label" for="optionsVerificationStatus2">Approved</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
    
                    <fieldset className="mb-20">
                      <h5 className="mb-20">Topics</h5>
    
                   
                      <div className="form-group hide" id="user-expert">
                        <label for="">Expert Topics</label>
                        <div className="input-group">
                          <div className="dropdown bootstrap-select show-tick flex-one" style={{"width":" 100%;"}}><select className="flex-one selectpicker show-tick" data-style="form-control border border-light py-10" multiple="" data-selected-text-format="count > 3" title="Select Expert Topics" data-size="5" data-width="100%" data-live-search="true" data-max-options="5" maxoptionstext="You reached the maximum number of topics!" tabindex="-98">
                            <option>Expert Mustard</option>
                            <option>Expert Ketchup</option>
                            <option>Expert Relish</option>
                            <option>Expert Onions</option>
                            <option>Expert Carots</option>
                            <option>Expert Apples</option>
                            <option>Expert Bananas</option>
                            <option>Expert Limes</option>
                            <option>Expert Vine</option>
                            <option>Expert Beer</option>
                            <option>Expert Water</option>
                          </select><button type="button" className="btn dropdown-toggle bs-placeholder form-control border border-light py-10" data-toggle="dropdown" role="button" title="Select Expert Topics"><div className="filter-option"><div className="filter-option-inner"><div className="filter-option-inner-inner">Select Expert Topics</div></div> </div></button><div className="dropdown-menu " role="combobox"><div className="bs-searchbox"><input type="text" className="form-control" autocomplete="off" role="textbox" aria-label="Search"/></div><div className="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul className="dropdown-menu inner show"></ul></div></div></div>
                          <div className="input-group-append pl-12">
                            <button className="btn btn-outline-primary rounded" type="button">Add</button>
                          </div>
                        </div>
                        <ul className="list-group list-group-flush mt-12">
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Expert Mustard</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Expert Ketchup</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Expert Relish</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Expert Onions</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Expert Bananas</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                        </ul>
                      </div>
    
                    
                      <div className="form-group">
                        <label for="">Follow Topics</label>
                        <div className="input-group">
                          <div className="dropdown bootstrap-select show-tick flex-one" style={{"width":" 100%;"}}><select className="flex-one selectpicker show-tick" data-style="form-control border border-light py-10" multiple="" data-selected-text-format="count > 3" title="Select Topics" data-size="5" data-width="100%" data-live-search="true" tabindex="-98">
                            <option>Mustard</option>
                            <option>Ketchup</option>
                            <option>Relish</option>
                            <option>Onions</option>
                            <option>Carots</option>
                            <option>Apples</option>
                            <option>Bananas</option>
                            <option>Limes</option>
                            <option>Vine</option>
                            <option>Beer</option>
                            <option>Water</option>
                          </select><button type="button" className="btn dropdown-toggle bs-placeholder form-control border border-light py-10" data-toggle="dropdown" role="button" title="Select Topics"><div className="filter-option"><div className="filter-option-inner"><div className="filter-option-inner-inner">Select Topics</div></div> </div></button><div className="dropdown-menu " role="combobox"><div className="bs-searchbox"><input type="text" className="form-control" autocomplete="off" role="textbox" aria-label="Search"/></div><div className="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul className="dropdown-menu inner show"></ul></div></div></div>
                          <div className="input-group-append pl-12">
                            <button className="btn btn-outline-primary rounded" type="button">Add</button>
                          </div>
                        </div>
                        <ul className="list-group list-group-flush mt-12">
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Mustard</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Ketchup</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Relish</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Onions</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                          <li className="list-group-item media d-flex px-0 px-xl-16">
                            <div className="media-body min-w-0">
                              <span className="text-primary text-truncate d-block">Bananas</span>
                            </div>
                            <div className="media-right">
                              <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                <i className="icon icon-close"></i>
                              </button>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </fieldset>
    
                    <fieldset className="mb-20">
                      <h5 className="mb-20">Media</h5>
                      
                  
                      <div className="form-group">
                        <label for="">Upload Images</label>
    
                        <fieldset className="mb-12">
                          <a href="javascript:void(0)" onclick="$('#pro-image').click()" className="btn btn-outline-primary btn-block">Upload Image</a>
                          <input type="file" id="pro-image" name="pro-image" style={{"display":"none;"}} className="form-control" multiple=""/>
                        </fieldset>
                        
                        <div className="preview-images">
                            <div className="card card-unstyled" id="no-files-container" style={{"display":"none;"}}>
                              <div className="card-block text-center pa-30 py-lg-40">
                                <i className="icon icon-32 icon-notification text-muted mb-20"></i>
                                <h6 className="mb-0">
                                  No results found for the selected filter.
                                </h6>
                              </div>
                            </div>
                            <div className="preview-images-zone ui-sortable">
                              <div className="preview-image preview-show-1 ui-sortable-handle">
                                <div className="image-cancel" data-no="1">
                                  <i className="icon icon-close"></i>
                                </div>
                                <div className="image-zone">
                                  <img id="pro-img-1" src="//static1.askaway.com/images/sample/profile-gallery/profile-thumb-1.jpg" alt="uploaded image 1"/>
                                </div>
                              </div>
    
                              <div className="preview-image preview-show-2 ui-sortable-handle">
                                  <div className="image-cancel" data-no="2">
                                    <i className="icon icon-close"></i>
                                  </div>
                                  <div className="image-zone">
                                    <img id="pro-img-1" src="//static1.askaway.com/images/sample/profile-gallery/profile-thumb-2.jpg" alt="uploaded image 2"/>
                                  </div>
                              </div>
    
                              <div className="preview-image preview-show-3 ui-sortable-handle">
                                  <div className="image-cancel" data-no="3">
                                    <i className="icon icon-close"></i>
                                  </div>
                                  <div className="image-zone">
                                    <img id="pro-img-3" src="//static1.askaway.com/images/sample/profile-gallery/profile-thumb-3.jpg" alt="uploaded image 3"/>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>
    
           <div className="form-group">
                        <label for="">
                          Intro Video
                        </label>
                        <label for="btnFileVideo" className="upload btn btn-md btn-block btn-outline-primary btn-file">
                          <input type="file" id="btnFileVideo" className="btn-file-input" placeholder="Choose file..."/>
                          <span className="btn-file-control">Browse Video ...</span>
                        </label>
                        <span className="form-text d-block">Videos require admin approval</span>
                      </div>
                    </fieldset>
                    
                  </form>
    
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#addAccountSuccess" data-dismiss="modal">
                      Add Account
                    </button>
                    <button type="button" className="btn btn-outline-light" data-toggle="modal" data-target="#addAccountError" data-dismiss="modal" aria-label="Close">
                      Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>

     );
    
    }
  }
  
  
  export default AddAccounts;
  