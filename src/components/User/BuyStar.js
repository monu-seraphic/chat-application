import React, { Component } from "react";
import $ from "jquery";
class Star extends Component {

  render() {
      return (
<div className="modal fade" id="buyStars" tabindex="-1" role="dialog" aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
        <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title">
                    Add More Stars
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <i className="icon icon-close"></i>
                </button>
            </div>
            <div className="modal-body">
            <div className="modal-body">

                
<div className="card bg-faded mb-30">
  <div className="card-block px-20 py-8">
    <div className="w-100 media align-items-center">
      <div className="media-left pr-8 pr-xl-12">
        <i className="icon icon-lg icon-star-circle d-inline-flex mt-2 text-warning"></i>
      </div>
      <div className="media-body">
        <span className="media-title text-xlarge font-weight-bold mb-0">500</span>
      </div>
      <div className="media-right">
        <span className="text-default text-lg-large">Current Balance</span>
      </div>
    </div>
  </div>
</div>

<h5>How Many Stars?</h5>
<div className="list-group mb-30">
  <label className="list-group-item list-group-item-action" for="defaultStarValue1">
    <div className="custom-control custom-radio">
      <input type="radio" id="defaultStarValue1" name="optionDefaultStarValue" className="custom-control-input" data-id="defaultStarValue1"></input>
      <div className="custom-control-label">250 ★</div>
    </div>
  </label>
  <label className="list-group-item list-group-item-action" for="defaultStarValue2">
    <div className="custom-control custom-radio">
      <input type="radio" id="defaultStarValue2" name="optionDefaultStarValue" className="custom-control-input" data-id="defaultStarValue2"></input>
      <div className="custom-control-label">750 ★</div>
    </div>
  </label>
  <label className="list-group-item list-group-item-action" for="defaultStarValue3">
    <div className="custom-control custom-radio">
      <input type="radio" id="defaultStarValue3" name="optionDefaultStarValue" className="custom-control-input" data-id="defaultStarValue3"></input>
      <div className="custom-control-label">1,000 ★</div>
    </div>
  </label>
</div>

<h5>Custom Amount</h5>
<div className="form-group">
  <input type="number" className="form-control form-control-lg" placeholder="Enter Amount"></input>
  <small className="form-text">Limited to Min. 50 — Max.1000 Stars</small>
</div>

</div>

            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary">
                Buy Stars
              </button>
              <button type="button" className="btn btn-outline-light" data-dismiss="modal" aria-label="Close">
                Cancel
              </button>
            </div>
        </div>
    </div>
</div>
     );
    
    }
  }
  
  
  export default Star;
  