import React, { Component } from "react";

class UnlockSingle extends Component {

  render() {
      return (
      
        <div className="modal fade" id="modalUnlockMediaSingle" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-sm modal-dialog-centered modal-dialog-zoom" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">
                          Unlock Media
                        </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <i className="icon icon-close"></i>
                        </button>
                    </div>
                    
                    <div className="modal-cover">
                      <figure className="message-media message-media-locked">
                        <div className="message-media-block">
                          <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{"backgroundImage": "url('{{STATIC_URL}}images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                            <div className="message-media-lock" style={{"backgroundImage": "url('{{STATIC_URL}}images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
                          </div>
                        </div>
                      </figure>
                    </div>
        
                    <div className="modal-body">
                      <h3 className="mb-0">
                        Photo Name
                        <small className="d-block text-muted mt-4">Unlock photo for 100 Stars</small>
                      </h3>
                    </div>
        
                    <div className="modal-footer">
                      <button type="button" className="btn btn-block btn-outline-light mb-8 mt-0" data-dismiss="modal" aria-label="Close">
                        Cancel
                      </button>
                      <button type="button" className="btn btn-block btn-primary mb-8 mt-0" data-toggle="modal" data-target="#modalViewMediaSingle" data-dismiss="modal">
                        Unlock
                      </button>
                    </div>
                </div>
            </div>
        </div>
       

        );
    
    }
  }
  
  
  export default UnlockSingle;
  