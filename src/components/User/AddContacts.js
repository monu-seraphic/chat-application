import React, { Component } from "react";

class AddContacts extends Component {

  render() {
      return (
        
        <div className="modal fade" id="addFriends" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom" role="document">
                <div className="modal-content min-fh-560">
                    <div className="modal-header">
                        <h5 className="modal-title">
                          Add Contact
                        </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <i className="icon icon-close"></i>
                        </button>
                    </div>
                    <div className="modal-body">
 
                        
                        <div className="grid grid-equal-height flex-one">
                        
                        <div className="grid-col-xl max-fw-xl-320 max-fw-lg-360 max-fw-xlg-440">
                          <div className="grid-fill h-100 border-right border-light no-border-right-xl-down pr-xl-30">
                            <form>
                              
                              <div className="form-group">
                                <label for="">Search</label>
                                <input type="text" className="form-control" placeholder="Search for Users"/>
                              </div>
        
                              <div className="form-group">
                                <label for="">Topics</label>
                                <select className="selectpicker show-tick" 
                                  data-style="form-control border border-light py-10" multiple data-selected-text-format="count > 3" title="Select" 
                                  data-size="5" data-live-search="true">
                                  <option>Mustard</option>
                                  <option>Ketchup</option>
                                  <option>Relish</option>
                                  <option>Onions</option>
                                  <option>Carots</option>
                                  <option>Apples</option>
                                  <option>Bananas</option>
                                  <option>Limes</option>
                                  <option>Vine</option>
                                  <option>Beer</option>
                                  <option>Water</option>
                                </select>
                              </div>
        
                              <div className="form-group">
                                <label>Expert</label>
                                <select className="selectpicker show-tick" 
                                  data-style="form-control border border-light py-10" title="Select" 
                                  data-size="3" data-width="fill">
                                  <option>All</option>
                                  <option>Yes</option>
                                  <option>No</option>
                                </select>
                              </div>
        
                              <div className="form-group">
                                <label>Gender</label>
                                <select className="selectpicker show-tick" 
                                  data-style="form-control border border-light py-10" title="Select" 
                                  data-size="3" data-width="fill">
                                  <option>All</option>
                                  <option>Male</option>
                                  <option>Female</option>
                                </select>
                              </div>
        
                              <div className="form-group">
                                <label>Location</label>
                                <select className="selectpicker show-tick" 
                                  data-style="form-control border border-light py-10" title="Select" 
                                  data-size="5" data-live-search="true">
                                  <option>Location 1</option>
                                  <option>Location 2</option>
                                  <option>Location 3</option>
                                  <option>Location 4</option>
                                  <option>Location 5</option>
                                  <option>Location 6</option>
                                  <option>Location 7</option>
                                  <option>Location 8</option>
                                  <option>Location 9</option>
                                  <option>Location 10</option>
                                  <option>Location 11</option>
                                </select>
<br></br>
<br></br>
                   <form classNameName="px-20 px-xl-30">
                                <label for="">Add to account</label>
  <div className="filter-option">
                    
                    <select className="selectpicker" data-live-search="true" data-style="form-control py-6 border border-light" data-size="5" data-width="fill">
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-1.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Roelof Bekkenenks</span></span>">Roelof Bekkenenks</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-4.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Tony McDonald</span></span>">Tony McDonald</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-5.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Eunice Cross</span></span>">Eunice Cross</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-3.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Udom Paowsong</span></span>">Udom Paowsong</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-2.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Sofía Alcocer</span></span>">Sofía Alcocer</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-6.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Teng Jiang</span></span>">Teng Jiang</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-8.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Torsten Paulsson</span></span>">Torsten Paulsson</option>
                      <option data-content="<span className='d-flex align-items-center min-w-0 max-fw-160'><figure className='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-7.jpg' className='rounded-circle' alt='image'></figure><span className='d-block py-2 text-truncate'>Iruka Akuchi</span></span>">Iruka Akuchi</option>
                    </select>
                  </div>

    
  </form>
                              </div>
                              
                            </form>
                          </div>
                        </div>
        
                        <div className="grid-col-xl flex-xl-one">
                          <div className="grid-fill d-flex h-100">
        
                        
                              <div className="card card-unstyled" style={{"display": "none"}}>
                                <div className="card-block text-center pa-30 py-lg-40">
                                  <i className="icon icon-32 icon-notification text-muted mb-16 d-inline-flex"></i>
                                  <h6 className="mb-0">
                                    No results found for your request.
                                  </h6>
                                </div>
                              </div>
                        
        
                              <div className="card-scroller-xlg">
                                <ul className="list-group list-group-flush list-group-chat">
                                    
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar avatar-gold">
                                                <img src="//static1.askaway.com/images/sample/user-basic-1.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                          <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                            <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck1" data-id="customCheck1"/>
                                            <label className="custom-control-label" for="customCheck1">
                                              <h5>Harrietta Souten</h5>
                                              <p>Dental Hygienist</p>
                                            </label>
                                          </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar avatar-gold">
                                                <span className="avatar-title bg-success rounded-circle">AM</span>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck2" data-id="customCheck2"/>
                                              <label className="custom-control-label" for="customCheck2">
                                                <h5>Aline McShee</h5>
                                                <p>Marketing Assistant</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar avatar-gold">
                                                <img src="//static1.askaway.com/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck3" data-id="customCheck3"/>
                                              <label className="custom-control-label" for="customCheck3">
                                                <h5>Brietta Blogg</h5>
                                                <p>Actuary</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar">
                                                <img src="//static1.askaway.com/images/sample/user-basic-3.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck4" data-id="customCheck4"/>
                                              <label className="custom-control-label" for="customCheck4">
                                                <h5>Karl Hubane</h5>
                                                <p>Chemical Engineer</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar avatar-gold">
                                                <img src="//static1.askaway.com/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck5" data-id="customCheck5"/>
                                              <label className="custom-control-label" for="customCheck5">
                                                <h5>Jillana Tows</h5>
                                                <p>Project Manager</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar">
                                                <span className="avatar-title bg-info rounded-circle">AD</span>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck6" data-id="customCheck6"/>
                                              <label className="custom-control-label" for="customCheck6">
                                                <h5>Alina Derington</h5>
                                                <p>Nurse</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar">
                                                <span className="avatar-title bg-warning rounded-circle">SK</span>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck7" data-id="customCheck7"/>
                                              <label className="custom-control-label" for="customCheck7">
                                                <h5>Stevy Kermeen</h5>
                                                <p>Associate Professor</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar avatar-gold">
                                                <img src="//static1.askaway.com/images/sample/user-basic-6.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck8" data-id="customCheck8"/>
                                              <label className="custom-control-label" for="customCheck8">
                                                <h5>Stevy Kermeen</h5>
                                                <p>Senior Quality Engineer</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar">
                                                <img src="//static1.askaway.com/images/sample/user-basic-7.jpg" className="rounded-circle" alt="image"></img>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck9" data-id="customCheck9"/>
                                              <label className="custom-control-label" for="customCheck9">
                                                <h5>Gloriane Shimmans</h5>
                                                <p>Web Designer</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>
        
                                    <li className="list-group-item">
                                        <div>
                                            <figure className="avatar">
                                                <span className="avatar-title bg-secondary rounded-circle">BP</span>
                                            </figure>
                                        </div>
                                        <div className="users-list-body">
                                            <div className="custom-control custom-checkbox custom-control-inverse mb-0">
                                              <input type="checkbox" className="custom-control-input" name="user-select" id="customCheck10" data-id="customCheck10" />
                                              <label className="custom-control-label" for="customCheck10">
                                                <h5>Bernhard Perrett</h5>
                                                <p>Software Engineer</p>
                                              </label>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                              </div>
                            
                          </div>
                        </div>
                      </div>
        
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary">
                          Add to Contacts
                        </button>
                        <button type="button" className="btn btn-outline-light" data-dismiss="modal" aria-label="Close">
                          Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>




   
        
     );
    
    }
  }
  
  
  export default AddContacts;
  