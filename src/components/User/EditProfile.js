import React, { Component } from "react";

class EditProfile extends Component {

  render() {
      return (
<div className="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div className="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
				<div className="modal-content">
						<div className="modal-header">
								<h5 className="modal-title">
									Edit Profile
								</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
										<i className="icon icon-close"></i>
								</button>
						</div>

            <div className="scrollable-x pl-30 pt-16 border-bottom border-light">
              <ul className="nav nav-tabs flex-nowrap mr-30" role="tablist">
                  <li className="nav-item mr-16 mr-xl-30">
                      <a className="nav-link text-nowrap px-0 active" 
                        data-toggle="tab" href="#editProfile-account" 
                        role="tab" aria-controls="account" aria-selected="true">
                          Account
                      </a>
                  </li>
                  <li className="nav-item mr-16 mr-xl-30">
                      <a className="nav-link text-nowrap px-0" 
                        data-toggle="tab" href="#editProfile-topics" 
                        role="tab" aria-controls="topics" aria-selected="false">
                          Topics
                      </a>
                  </li>
                  <li className="nav-item mr-16 mr-xl-30">
                      <a className="nav-link text-nowrap px-0" 
                        data-toggle="tab" href="#editProfile-media" 
                        role="tab" aria-controls="media" aria-selected="false">
                          Media
                      </a>
                  </li>
                  <li className="nav-item mr-16 mr-xl-30">
                      <a className="nav-link text-nowrap px-0" 
                        data-toggle="tab" href="#editProfile-verify" 
                        role="tab" aria-controls="verification" aria-selected="false">
                          Verification
                      </a>
                  </li>
              </ul>
            </div>
						
            <div className="modal-body pt-16 pb-0">
								<div className="tab-content pt-0">
										
                    <div className="tab-pane show active" id="editProfile-account" role="tabpanel">
											<form action="">

                        <fieldset className="mb-20">
                          <h5 className="mb-20">Account Details</h5>
                         
                          <div className="form-group">
                            <label for="">Username</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="icon icon-user-alt-outline"></i>
                                </span>
                              </div>
                              <input type="text" className="form-control" placeholder="Add Username"></input>
                            </div>
                          </div>

                         
                          <div className="form-group">
                            <label for="">Password</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="icon icon-password"></i>
                                </span>
                              </div>
                              <input type="password" className="form-control" placeholder="Enter Password"></input>
                            </div>
                          </div>

                        
                          <div className="form-group">
                            <label for="">Repeat Password</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="icon icon-onward"></i>
                                </span>
                              </div>
                              <input type="password" className="form-control" placeholder="Confirm Password"></input>
                            </div>
                          </div>
                        </fieldset>

                        <fieldset className="mb-20">
                          <h5 className="mb-20">General</h5>

                        
                          <div className="form-group">
                            <label for="">Full Name</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="icon icon-user"></i>
                                </span>
                              </div>
                              <input type="text" className="form-control" placeholder="Your Name"></input>
                            </div>
                          </div>

                      
                          <div className="form-group">
                            <label>Location</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="icon icon-location-outline"></i>
                                </span>
                              </div>
                              <input type="text" className="form-control" placeholder="Add Location"></input>
                            </div>
                          </div>

                          
                          <div className="form-group">
                            <label for="">Biography</label>
                            <textarea rows="5" cols="" className="form-control" placeholder="Add short description"></textarea>
                          </div>

                          <div className="grid grid-narrow">
                            <div className="grid-col-xl">
                              
                            
                              <div className="form-group">
                                <label>User Type</label>
                                <select className="selectpicker show-tick" name="edit-user-type" 
                                  data-style="form-control border border-light py-10" title="Select" 
                                  data-size="3" data-width="fill">
                                  <option value="edit-user-normal">Normal</option>
                                  <option value="edit-user-pro">Pro</option>
                                  <option value="edit-user-expert">Expert</option>
                                </select>
                              </div>

                            </div>
                            <div className="grid-col-xl">

                            
                              <div className="form-group">
                                <label>Gender</label>
                                <select className="selectpicker show-tick" 
                                  data-style="form-control border border-light py-10" title="Select" 
                                  data-size="2" data-width="fill">
                                  <option>Male</option>
                                  <option>Female</option>
                                </select>
                              </div>

                            </div>
                          </div>

                          <div className="form-group hide" id="edit-user-pro">
                            <label>Verification Status</label>
                            <div className="card">
                              <div className="card-block py-8 px-12">
                                <div className="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="optionsEditVerificationStatus1" name="optionsEditVerificationStatus" className="custom-control-input" checked></input>
                                  <label className="custom-control-label" for="optionsEditVerificationStatus1">Pending Review</label>
                                </div>
                                <div className="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="optionsEditVerificationStatus2" name="optionsEditVerificationStatus" className="custom-control-input"></input>
                                  <label className="custom-control-label" for="optionsEditVerificationStatus2">Approved</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                      </form>
										</div>

										<div className="tab-pane" id="editProfile-topics" role="tabpanel">
											<fieldset className="mb-20">
                        <h5 className="mb-20">Topics</h5>

                       
                        <div className="form-group hide" id="edit-user-expert">
                          <label for="">Expert Topics</label>
                          <div className="input-group">
                            <select className="flex-one selectpicker show-tick" 
                              data-style="form-control border border-light py-10" multiple 
                              data-selected-text-format="count > 3" title="Select Expert Topics" data-size="5" data-width="100%"
                              data-live-search="true" data-max-options="5" maxOptionsText="You reached the maximum number of topics!">
                              <option>Expert Mustard</option>
                              <option>Expert Ketchup</option>
                              <option>Expert Relish</option>
                              <option>Expert Onions</option>
                              <option>Expert Carots</option>
                              <option>Expert Apples</option>
                              <option>Expert Bananas</option>
                              <option>Expert Limes</option>
                              <option>Expert Vine</option>
                              <option>Expert Beer</option>
                              <option>Expert Water</option>
                            </select>
                            <div className="input-group-append pl-12">
                              <button className="btn btn-outline-primary rounded" type="button">Add</button>
                            </div>
                          </div>
                          <ul className="list-group list-group-flush mt-12">
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Expert Mustard</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Expert Ketchup</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Expert Relish</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Expert Onions</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Expert Bananas</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                          </ul>
                        </div>

                       
                        <div className="form-group">
                          <label for="">Follow Topics</label>
                          <div className="input-group">
                            <select className="flex-one selectpicker show-tick" 
                              data-style="form-control border border-light py-10" multiple data-selected-text-format="count > 3" title="Select Topics" 
                              data-size="5" data-width="100%" data-live-search="true">
                              <option>Mustard</option>
                              <option>Ketchup</option>
                              <option>Relish</option>
                              <option>Onions</option>
                              <option>Carots</option>
                              <option>Apples</option>
                              <option>Bananas</option>
                              <option>Limes</option>
                              <option>Vine</option>
                              <option>Beer</option>
                              <option>Water</option>
                            </select>
                            <div className="input-group-append pl-12">
                              <button className="btn btn-outline-primary rounded" type="button">Add</button>
                            </div>
                          </div>
                          <ul className="list-group list-group-flush mt-12">
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Mustard</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Ketchup</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Relish</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Onions</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                            <li className="list-group-item media d-flex px-0 px-xl-16">
                              <div className="media-body min-w-0">
                                <span className="text-primary text-truncate d-block">Bananas</span>
                              </div>
                              <div className="media-right">
                                <button className="btn btn-link btn-link-secondary pa-0" type="button">
                                  <i className="icon icon-close"></i>
                                </button>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </fieldset>
										</div>

										<div className="tab-pane" id="editProfile-media" role="tabpanel">
											<fieldset className="mb-20">
                        <h5 className="mb-20">Media</h5>
                        
                     
                        <div className="form-group">
                          <label for="">Upload Images</label>

                          <fieldset className="mb-12">
                            <a href="javascript:void(0)" onclick="$('#pro-image').click()" className="btn btn-outline-primary btn-block">Upload Image</a>
                            <input type="file" id="pro-image" name="pro-image" style={{"display": "none"}} className="form-control" multiple></input>
                          </fieldset>
                          
                          <div className="preview-images">
                              <div className="card card-unstyled" id="no-files-container" style={{display:"none"}}>
                                <div className="card-block text-center pa-30 py-lg-40">
                                  <i className="icon icon-32 icon-notification text-muted mb-20"></i>
                                  <h6 className="mb-0">
                                    No results found for the selected filter.
                                  </h6>
                                </div>
                              </div>
                              <div className="preview-images-zone">
                                <div className="preview-image preview-show-1">
                                  <div className="image-cancel" data-no="1">
                                    <i className="icon icon-close"></i>
                                  </div>
                                  <div className="image-zone">
                                    <img id="pro-img-1" src="{{STATIC_URL}}images/sample/profile-gallery/profile-thumb-1.jpg" alt="uploaded image 1"></img>
                                  </div>
                                </div>

                                <div className="preview-image preview-show-2">
                                    <div className="image-cancel" data-no="2">
                                      <i className="icon icon-close"></i>
                                    </div>
                                    <div className="image-zone">
                                      <img id="pro-img-1" src="{{STATIC_URL}}images/sample/profile-gallery/profile-thumb-2.jpg" alt="uploaded image 2"></img>
                                    </div>
                                </div>

                                <div className="preview-image preview-show-3">
                                    <div className="image-cancel" data-no="3">
                                      <i className="icon icon-close"></i>
                                    </div>
                                    <div className="image-zone">
                                      <img id="pro-img-3" src="{{STATIC_URL}}images/sample/profile-gallery/profile-thumb-3.jpg" alt="uploaded image 3"></img>
                                    </div>
                                </div>
                              </div>
                          </div>
                        </div>

                     
                        <div className="form-group">
                          <label for="">
                            Intro Video
                          </label>
                          <label for="btnFileVideo" className="upload btn btn-md btn-block btn-outline-primary btn-file">
                            <input type="file" id="btnFileVideo" className="btn-file-input" placeholder="Choose file..."></input>
                            <span className="btn-file-control">Browse Video ...</span>
                          </label>
                          <span className="form-text d-block">Videos require admin approval</span>
                        </div>
                      </fieldset>
										</div>

										<div className="tab-pane" id="editProfile-verify" role="tabpanel">
                        <fieldset className="mb-20">
                            <h5 className="mb-20">Account Verification</h5>
														<div className="form-group">
																<div className="input-group">
                                    <div className="input-group-prepend">
																				<span className="input-group-text bg-facebook">
																						<i className="icon icon-facebook"></i>
																				</span>
																		</div>
																		<input type="text" className="form-control is-valid" placeholder="Username" value="username@gmail.com"></input>
																</div>
														</div>
														<div className="form-group">
																<div className="input-group">
                                    <div className="input-group-prepend">
																				<span className="input-group-text bg-twitter">
																						<i className="icon icon-twitter"></i>
																				</span>
																		</div>
																		<input type="text" className="form-control is-valid" placeholder="Username" value="@twitterUser69"></input>
																</div>
														</div>
														<div className="form-group">
																<div className="input-group">
                                    <div className="input-group-prepend">
																				<span className="input-group-text bg-instagram">
																						<i className="icon icon-instagram"></i>
																				</span>
																		</div>
																		<input type="text" className="form-control is-valid" placeholder="Username" value="instaUserName"></input>
																</div>
														</div>
														<div className="form-group">
																<div className="input-group">
                                    <div className="input-group-prepend">
																				<span className="input-group-text bg-linkedin">
																						<i className="icon icon-linkedin"></i>
																				</span>
																		</div>
																		<input type="text" className="form-control" placeholder="Username"></input>
																</div>
														</div>
                        </fieldset>
										</div>

								</div>
						</div>
						<div className="modal-footer">
                <button type="button" className="btn btn-light" data-dismiss="modal">Cancel</button>
								<button type="button" className="btn btn-primary">Save</button>
						</div>
				</div>
		</div>
</div>
      
      
      );
    
}
}


export default EditProfile;
