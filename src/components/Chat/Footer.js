import React, { Component } from "react";

class ChatFooter extends Component {

  render() {
      return (
        <div className="chat-footer">
  <form className="flex-wrap flex-xl-nowrap">
      <div className="input-actions-mobile w-100 collapse" id="input-actions-mobile">
        <nav className="nav mb-8">
          <a className="nav-link px-12 active" href="#">
            <i className="icon icon-face-smile icon-md"></i>
          </a>
          <a className="nav-link px-12" href="#">
            <i className="icon icon-attachment icon-md"></i>
          </a>
        </nav>
      </div>

      <div className="form-buttons hidden-xs-down">
        <button className="btn btn-outline-light px-10 px-xl-12 mr-4 mr-md-8" data-toggle="tooltip" title="Emoji" type="button" id="chat-emoji-button">
            <i className="icon icon-face-smile icon-md"></i>
        </button>
        <button className="btn btn-outline-light px-10 px-xl-12 mr-4 mr-md-8" data-toggle="tooltip" title="Add files" type="button">
            <i className="icon icon-attachment icon-md"></i>
        </button>
      </div>
      
      <div className="form-buttons mr-8 hidden-xl-up">
        <button className="btn btn-outline-light px-10" type="button" 
            data-toggle="collapse" data-target="#input-actions-mobile">
            <i className="icon icon-plus icon-md text-primary"></i>
        </button>
      </div>

      <div className="input-control">
        <input type="text" className="form-control" placeholder="Write a message." id="chat-input-area" />
        <div className="input-control-stats">

          <span className="input-control-stats-item">
            <i className="icon icon-star-circle"></i> 888
          </span>
          <span className="input-control-stats-item">
            <i className="icon icon-chat-bubble"></i> <strong>0</strong> | 150
          </span>
        </div>
      </div>
      
      <div className="form-buttons">    
        <button className="btn btn-primary px-10 px-xl-12 min-fw-xl-40 min-fw-xlg-60" type="submit">
            <i className="icon icon-messages icon-md mx-auto"></i>
        </button>
      </div>
  </form>
</div>
       
       );
    
  }
}


export default ChatFooter;
