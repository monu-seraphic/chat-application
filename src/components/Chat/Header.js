import React, { Component } from "react";

class ChatHeader extends Component {

  render() {
      return (
        <header className="chat-header">
  <div className="chat-header-block">
    <div className="chat-header-action d-xlg-none">
      <a href="#" className="btn btn-link btn-link-secondary mobile-navigation-button px-0 mr-12 ml-n-12 ml-xlg-0">
        <i className="icon icon-lg icon-menu"></i>
      </a>
    </div>

    <div className="chat-header-user flex-one">

      <div className="chat-header-user-item">
        <figure className="avatar fw-30 fh-30 mx-0">
          <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
        </figure
        ><a className="btn btn-link-secondary py-0 no-border no-shadow" href="#" data-navigation-target="contact-information">
          <h5 className="mb-0 text-truncate text-left">
            Byrom Guittet
            <small className="d-block font-primary-re mt-4">Star Mode Enabled</small>
          </h5>
        </a>
      </div>

      <div className="chat-header-user-item ml-auto hidden-xs-down">
        <a className="btn btn-link-secondary no-border no-shadow" href="#" data-navigation-target="my-profile">
          <h5 className="mb-0 text-truncate">Mirabelle Tow</h5>
        </a
        ><figure className="avatar fw-30 fh-30 mx-0">
          <img src="/images/sample/user-basic-4.jpg" className="rounded-circle" alt="image" />
        </figure>
      </div>

    </div>
  </div>
  <div className="chat-header-toolbar justify-content-end mx-n-12 mx-xl-0">
    <span className="badge badge-pill bg-grey mr-auto text-default font-weight-normal">
      &#9733; 888
    </span>

    <div className="form-item custom-control custom-switch">
      <input type="checkbox" className="custom-control-input" id="customSwitchStarMode" />
      <label className="custom-control-label" for="customSwitchStarMode">
        <span className="hidden-xs-down">Enable</span> Star Mode
      </label>
    </div>

    <a href className="badge badge-pill badge-primary text-default font-weight-normal ml-12 ml-xl-16"
      data-toggle="modal" data-target="#buyStars">
      &#9733; 888
    </a>
  </div>
</header>
       
       );
    
  }
}


export default ChatHeader;
