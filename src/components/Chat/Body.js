import React, { Component } from "react";

class ChatBody extends Component {

  render() {
      return (
        <div className="chat-body">
  <div className="messages">

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-warning"></i>
          </span>
          <div>
            <h6 className="message-title">
              System Notification
            </h6>
            <p className="text-small">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum labore error nobis quibusdam cupiditate sit quisquam! Fugit, vitae excepturi. Doloremque quas suscipit cum temporibus ad nisi sequi ipsum impedit consequuntur.
            </p>
          </div>
        </div>
      </div>

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-warning"></i>
          </span>
          <div>
            <h6 className="message-title">
              You have blocked Username.
            </h6>
            <p className="text-small">
              To start chatting with them, remove them from your blocked list.
            </p>
          </div>
        </div>
      </div>

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-blocked"></i>
          </span>
          <div>
            <h6 className="message-title text-center">
              Username has blocked you.
            </h6>
          </div>
        </div>
      </div>

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-warning"></i>
          </span>
          <div>
            <h6 className="message-title">
              <span className="text-primary">AntonDubkov</span> just enabled star mode.
            </h6>
            <p className="text-small">
              In star mode with Anton, each message you send costs 100 stars.
              You currently do not have any stars.
            </p>
            <p className="text-small">
              Star mode gives Anton an incentive to respond to your messages and to offer meaningful advice and engagement.
            </p>
          </div>
        </div>
      </div>

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-warning"></i>
          </span>
          <div>
            <h6 className="message-title">
              You activated Star Mode.
            </h6>
            <p className="text-small">
              Messages from AntonDubkov now cost them 100 stars each.
            </p>
          </div>
        </div>
      </div>

      <div className="message-item system-message">
        <div className="message-content">
          <span className="message-icon">
            <i className="icon icon-delivery-warning"></i>
          </span>
          <div>
            <h6 className="message-title">
              You turned off Star Mode.
            </h6>
            <p className="text-small">
              All messages that AntonDubkov now sends are now free until Star Mode is turned on again.
            </p>
          </div>
        </div>
      </div>
      
      <div className="message-item outgoing-message">
        <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
        <div className="message-content">
          <div>
            Hello how are you?
          </div>
          <a href="" className="message-translate">
            <i className="icon icon-translate"></i>
          </a>
        </div>
        <div className="time"><i className="icon icon-delivery-success text-info"></i> 01:20 PM</div>
      </div>
      
      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">01:35 PM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content">
              <div>
                I'm fine, how are you 😃
              </div>
              <a href className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
      </div>
      
      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="message-content">
              <div>
                I'm fine thank you. I expect you to send me some files.
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 05:31 PM</div>
      </div>
      
      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">10:12 PM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content">
            <div>
              What files are you talking about? I'm sorry I can't remember right now.
            </div>
            <a href="" className="message-translate">
              <i className="icon icon-translate"></i>
            </a>
          </div>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="message-content">
              <div>
                I want those files for you. I want you to send 1 PDF and 1 image file.
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 11:56 PM</div>
      </div>
      
      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">11:59 PM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content message-file">
            <div>
                <div className="file-icon">
                    <i className="fa fa-file-pdf-o"></i>
                </div>
                <div>
                    <div>important_documents.pdf <i className="text-muted small">(50KB)</i></div>
                    <ul className="list-inline">
                        <li className="list-inline-item mb-0"><a href="#">Download</a></li>
                        <li className="list-inline-item mb-0"><a href="#">View</a></li>
                    </ul>
                </div>
            </div>
            <a href="" className="message-translate">
              <i className="icon icon-translate"></i>
            </a>
          </div>
      </div>

      <div class="message-item messages-divider sticky-top" data-label="Today"></div>
      
      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">07:15 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content">
            <div>
              I'm about to send the other file now.
            </div>
            <a href="" className="message-translate">
              <i className="icon icon-translate"></i>
            </a>
          </div>
          <span className="cost">100 &#9733;</span>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="message-content">
              <div>
                Thank you so much. These files are very important to me. I guess you didn't make any changes
                to these files. So I need the original versions of these files. Thank you very much again.
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 07:45 AM</div>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">08:00 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content">
            <div>
              I thank you. We are glad to help you. 😃
            </div>
            <a href="" className="message-translate">
              <i className="icon icon-translate"></i>
            </a>
          </div>
          <span className="cost">100 &#9733;</span>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="message-content">
              <div>
                😃 😃 ❤ ❤
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 09:23 AM</div>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">10:43 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media message-media-locked">
            <div className="message-media-block" data-toggle="modal" data-target="#modalUnlockMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}}>
                <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}}></div>
              </div>
            </div>
            <span className="cost">100 &#9733;</span>
          </figure>
          <figure className="message-media">
            <div className="message-media-block" data-toggle="modal" data-target="#modalViewMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-large-4.jpg')"}}>
              </div>
            </div>
          </figure>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">10:43 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media">
            <div className="message-media-block" data-toggle="modal" data-target="#modalViewMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-large-6.jpg')"}}>
              </div>
            </div>
            <div className="message-content">
              <div>
                I sent you all the files. Good luck with 😃, 
                I sent you all the files. Good luck with!
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
          </figure>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">10:43 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media">
            <div className="message-media-block message-media-video" data-video-lenght="03:20" data-toggle="modal" data-target="#modalViewMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-large-5.jpg')"}}>
                
              </div>
            </div>
          </figure>
          <figure className="message-media message-media-locked">
            <div className="message-media-block message-media-video" data-video-lenght="04:35" data-toggle="modal" data-target="#modalUnlockMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}} >
                <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}} ></div>
              </div>
            </div>
            <div className="message-content">
              <div>
                Here is my video, Mate!
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
            <span className="cost">100 &#9733;</span>
          </figure>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <figure className="message-media message-media-locked">
            <div className="message-media-block" data-toggle="modal" data-target="#modalUnlockMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-2.jpg')"}} >
                <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-2.jpg')"}} ></div>
              </div>
            </div>
          </figure>
          <div className="message-content">
              <div>
                You are good ❤❤
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
          </div>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 10:50 AM</div>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <figure className="message-media">
            <div className="message-media-block message-media-video" data-video-lenght="03:20" data-toggle="modal" data-target="#modalViewMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-large-3.jpg')"}}>
                
              </div>
            </div>
          </figure>
          <figure className="message-media message-media-locked">
            <div className="message-media-block message-media-video" data-video-lenght="04:35" data-toggle="modal" data-target="#modalUnlockMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-3.jpg')"}}>
                <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-3.jpg')"}}></div>
              </div>
            </div>
            <div className="message-content">
              <div>
                Here is my video, Mate!
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
          </figure>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 10:50 AM</div>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <figure className="message-media message-media-group">
            <div className="message-media-block" data-toggle="modal" data-target="#modalViewMedia">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-10.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-11.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-12.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1 bg-black">
                  <span className="media-group-count" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-15.jpg')"}}>
                  </span>
                  <span>+5</span>
                </div>
              </div>
            </div>
          </figure>

          <figure className="message-media message-media-locked">
            <div className="message-media-block message-media-video" data-video-lenght="04:35" data-toggle="modal" data-target="#modalUnlockMediaSingle">
              <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-3.jpg')"}}>
                <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-3.jpg')"}}></div>
              </div>
            </div>
            <div className="message-content">
              <div>
                Here is my video, Mate!
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
          </figure>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 10:50 AM</div>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <figure className="message-media message-media-group message-media-locked" data-toggle="modal" data-target="#modalUnlockMediaGroup">
            <div className="message-media-block">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-16.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-16.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item last">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1">
                  <span className="media-group-count" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-15.jpg')"}} >
                  </span>
                  <span>+3</span>
                </div>
              </div>
            </div>
            <div className="message-content">
              <div>
                Here is my video, Mate!
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
          </figure>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 10:50 AM</div>
      </div>

      <div className="message-item outgoing-message">
          <div className="message-avatar">
              <figure className="avatar avatar-sm mr-0">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <figure className="message-media message-media-group message-media-locked" data-toggle="modal" data-target="#modalUnlockMediaGroup">
            <div className="message-media-block">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}}>
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}}></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}}>
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}}></div>
                </div>
              </div>
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-16.jpg')"}}>
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-16.jpg')"}}></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-2.jpg')"}}>
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-5.jpg')"}}></div>
                </div>
              </div>
            </div>
          </figure>
          <div className="time"><i className="icon icon-delivery-success text-info"></i> 10:50 AM</div>
      </div>

      <div class="message-item messages-divider sticky-top" data-label="1 message unread"></div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">11:05 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <div className="message-content">
            <div>
              I sent you all the files. Good luck with 😃
            </div>
            <a href="" className="message-translate">
              <i className="icon icon-translate"></i>
            </a>
          </div>
          <span className="cost">100 &#9733;</span>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">11:05 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media message-media-group" data-toggle="modal" data-target="#modalViewMedia">
            <div className="message-media-block">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-5.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-10.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-16.jpg')"}}>
                  
                </div>
              </div>
              <div className="message-media-block-item last">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1">
                  <span className="media-group-count" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-thumb-15.jpg')"}}>
                  </span>
                  <span>+3</span>
                </div>
              </div>
            </div>
            <span className="cost">100 &#9733;</span>
          </figure>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">11:05 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media message-media-group message-media-locked" data-toggle="modal" data-target="#modalUnlockMediaGroup">
            <div className="message-media-block">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-1.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-1.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-8.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-8.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item last">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1">
                  <span className="media-group-count" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}} >
                  </span>
                  <span>+1</span>
                </div>
              </div>
            </div>
            <div className="message-content">
              <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum quas aperiam accusantium omnis et magnam quis.
              </div>
              <a href="" className="message-translate">
                <i className="icon icon-translate"></i>
              </a>
            </div>
            <span className="cost">500 &#9733;</span>
          </figure>
      </div>

      <div className="message-item">
          <div className="message-avatar">
              <figure className="avatar">
                  <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
              </figure>
              <div>
                  <h5>Byrom Guittet</h5>
                  <div className="time">11:05 AM<i className="icon icon-delivery-sent text-info"></i></div>
              </div>
          </div>
          <figure className="message-media message-media-group message-media-locked">
            <div className="message-media-block" data-toggle="modal" data-target="#modalUnlockMediaGroup">
              <div className="message-media-block-item message-media-video" data-video-lenght="04:35">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-1.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-1.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-8.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-8.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-7.jpg')"}} ></div>
                </div>
              </div>
              <div className="message-media-block-item message-media-video" data-video-lenght="05:01">
                <div className="message-media-thumb aspect-ratio aspect-ratio-1by1" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}} >
                  <div className="message-media-lock" style={{backgroundImage: "url('/images/sample/profile-gallery/profile-pix-10.jpg')"}} ></div>
                </div>
              </div>
            </div>
            <span className="cost">400 &#9733;</span>
          </figure>
      </div>

  </div>
</div>
       
       );
    
  }
}


export default ChatBody;
