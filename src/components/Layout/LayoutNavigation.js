import React, { Component } from "react";
import $ from "jquery";

class LayoutNavigation extends Component {

  render() {
      return (
        <div className="nav-group">
          <ul>
            
            <li className="logo">
                <a href="#">
                  <img src="/images/brand/aa-logo-default.svg" className="img-fluid" alt="" />
                </a>
            </li>

            <li className="list-inline-item hidden-xlg-up d-inline">
                <a href="#" className="mobile-navigation-button justify-content-start justify-content-lg-center"
                  data-toggle="tooltip" title="Close" data-placement="right">
                    <i className="icon icon-lg icon-close mx-16 mx-lg-0"></i>
                    <span className="flex-one text-left py-2 hidden-lg-up">
                      Close
                    </span>
                </a>
            </li>

            <li>
              <a 
                className="active" 
                href="#" 
                data-toggle="tooltip" 
                title="Convos" 
                data-placement="right"  
                data-navigation-target="chats" 
              >
                <i className="icon icon-lg icon-comments mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Conversations
                </span>
              </a>
            </li>

            <li>
              <a data-navigation-target="discover" href="#" 
                data-toggle="tooltip" title="Discover" data-placement="right">
                <i className="icon icon-lg icon-search-outline mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Discover
                </span>
              </a>
            </li>

            <li>
              <a data-navigation-target="contacts" href="#" 
                data-toggle="tooltip" title="Contacts" data-placement="right">
                <i className="icon icon-lg icon-user mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Contacts
                </span>
              </a>
            </li>

            <li>
              <a data-navigation-target="favorites" href="#"
                data-toggle="tooltip" title="Favorites" data-placement="right">
                <i className="icon icon-lg icon-heart mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Favorites
                </span>
              </a>
            </li>

            <li>
              <a data-navigation-target="archived" href="#" 
                data-toggle="tooltip" title="Archives" data-placement="right">
                <i className="icon icon-lg icon-archive mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Archive
                </span>
              </a>
            </li>

            <li>
      <a data-navigation-target="my-accounts" href="#" data-toggle="tooltip" title="" data-placement="right" data-original-title="My Accounts" class="active">
        <i class="icon icon-lg icon-user-alt-outline mx-16 mx-lg-0"></i>
        <span class="flex-one text-left py-2 hidden-lg-up">
          My Accounts
        </span>
      </a>
    </li>
            <li>
              <a href="#" 
                data-toggle="tooltip" title="logout" data-placement="right">
                <i className="icon icon-lg icon-switch text-danger mx-16 mx-lg-0"></i>
                <span className="flex-one text-left py-2 hidden-lg-up">
                  Logout
                </span>
              </a>
            </li>

          </ul>
        </div>
       
       );
    
  }
}


export default LayoutNavigation;
