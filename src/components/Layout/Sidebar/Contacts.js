import React, { Component } from "react";

class Contacts extends Component {

  render() {
      return (
        <div id="contacts" className="sidebar">
    <header>
      <span className="flex-one flex-xlg-auto">
        Contacts
      </span>
      <ul className="list-inline">
        <li className="list-inline-item" data-toggle="tooltip" title="Add Contact">
          <a className="btn btn-outline-light" href="#" data-toggle="modal" data-target="#addFriends">
            <i className="icon icon-plus text-primary"></i>
          </a>
        </li>

        <li className="list-inline-item d-xlg-none d-inline">
          <a href="#" className="btn btn-outline-light text-danger sidebar-close">
            <i className="icon icon-close"></i>
          </a>
        </li>
      </ul>
    </header>
        
    <form className="px-20 px-xl-30 pb-0">
          <div class="form-group">
                    
                        <select class="selectpicker" data-live-search="true" data-style="form-control py-6 border border-light" data-size="5" data-width="fill">
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-1.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Roelof Bekkenenks</span></span>">Roelof Bekkenenks</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-4.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Tony McDonald</span></span>">Tony McDonald</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-5.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Eunice Cross</span></span>">Eunice Cross</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-3.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Udom Paowsong</span></span>">Udom Paowsong</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-2.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Sofía Alcocer</span></span>">Sofía Alcocer</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-6.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Teng Jiang</span></span>">Teng Jiang</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-8.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Torsten Paulsson</span></span>">Torsten Paulsson</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-7.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Iruka Akuchi</span></span>">Iruka Akuchi</option>
                        </select>
                      </div>
                      </form>
    <div className="px-20 px-xl-30 pt-30 pt-xlg-12">
      <ul className="nav nav-tabs" role="tablist">
        <li className="nav-item flex-one">
          <a className="nav-link text-center active" href=""
            data-toggle="tab" data-target="#contacts-following" 
            role="tab" aria-controls="contacts-following" aria-selected="true">
            Following
          </a>
        </li>
        <li className="nav-item flex-one">
          <a className="nav-link text-center" href=""
            data-toggle="tab" data-target="#contacts-followers" 
            role="tab" aria-controls="contacts-followers" aria-selected="false">
            Followers
          </a>
        </li>
      </ul>
    </div>

    <form className="px-20 px-xl-30">
      <input type="text" className="form-control" placeholder="Search" />
    </form>

    <div className="sidebar-body tab-content pt-0">
      
      <section className="tab-pane active show" id="contacts-following">

        <div style={{"display": "none"}}>
          <div className="card card-unstyled">
            <div className="card-block text-center pa-30 py-lg-40">
              <i className="icon icon-32 icon-notification text-muted mb-20"></i>
              <h6 className="mb-0">
                No results found for the selected filter.
              </h6>
            </div>
          </div>
        </div>
        <ul className="list-group list-group-flush">
            
            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-1.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Harrietta Souten</h5>
                        <p>Dental Hygienist</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-success rounded-circle">A</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Aline McShee</h5>
                        <p>Marketing Assistant</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Brietta Blogg</h5>
                        <p>Actuary</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <img src="/images/sample/user-basic-3.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Karl Hubane</h5>
                        <p>Chemical Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Jillana Tows</h5>
                        <p>Project Manager</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <span className="avatar-title bg-info rounded-circle">AD</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Alina Derington</h5>
                        <p>Nurse</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <span className="avatar-title bg-warning rounded-circle">SK</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Stevy Kermeen</h5>
                        <p>Associate Professor</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-6.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Stevy Kermeen</h5>
                        <p>Senior Quality Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <img src="/images/sample/user-basic-7.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Gloriane Shimmans</h5>
                        <p>Web Designer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <span className="avatar-title bg-secondary rounded-circle">BP</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Bernhard Perrett</h5>
                        <p>Software Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
      </section>

      <section className="tab-pane" id="contacts-followers">
        <div style={{"display": "none"}}>
          <div className="card card-unstyled">
            <div className="card-block text-center pa-30 py-lg-40">
              <i className="icon icon-32 icon-notification text-muted mb-20"></i>
              <h6 className="mb-0">
                No results found for the selected filter.
              </h6>
            </div>
          </div>
        </div>
        <ul className="list-group list-group-flush">
            
            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <img src="/images/sample/user-basic-8.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Barbara Hammermann</h5>
                        <p>Dental Hygienist</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-info rounded-circle">BM</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Bob McFerrin</h5>
                        <p>Marketing Assistant</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Johanna Campbridge</h5>
                        <p>Actuary</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-8.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Karl Hubane</h5>
                        <p>Chemical Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Thomas Thornstend</h5>
                        <p>Project Manager</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <span className="avatar-title bg-success rounded-circle">JD</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>James Derington</h5>
                        <p>Nurse</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-warning rounded-circle">JH</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>James Hampermann</h5>
                        <p>Associate Professor</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar">
                        <img src="/images/sample/user-basic-1.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Camerron SteinnHammmer</h5>
                        <p>Senior Quality Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-4.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Hamper Hampernamm Shimmans</h5>
                        <p>Web Designer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-warning rounded-circle">JH</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Jody Hammerhard</h5>
                        <p>Software Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-danger rounded-circle">DC</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Dior Cristiansenn</h5>
                        <p>Associate Professor</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-7.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Boy Hammster</h5>
                        <p>Senior Engineer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Shammer Hammersmith</h5>
                        <p>Fashion Designer</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li className="list-group-item" data-navigation-target="contact-information">
                <div>
                    <figure className="avatar avatar-gold">
                        <span className="avatar-title bg-danger rounded-circle">CH</span>
                    </figure>
                </div>
                <div className="users-list-body">
                    <div>
                        <h5>Carla Hampermann</h5>
                        <p>Diva</p>
                    </div>
                    <div className="users-list-action">
                        <div className="action-toggle">
                            <div className="dropdown">
                                <a data-toggle="dropdown" href="#">
                                    <i className="icon icon-more"></i>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <a href="#" className="dropdown-item">New chat</a>
                                    <a href="#" data-navigation-target="contact-information"
                                        className="dropdown-item">Profile</a>
                                    <div className="dropdown-divider"></div>
                                    <a href="#" className="dropdown-item text-danger">Block</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
      </section>

    </div>
</div>
       
       );
    
  }
}


export default Contacts;
