import React, { Component } from "react";

class Discover extends Component {

  render() {
      return (
        <div id="discover" className="sidebar">
  <header>
      <span className="flex-one flex-xlg-auto">
        Discover
      </span>
      <ul className="list-inline">
        
        <li className="list-inline-item d-inline d-xlg-none ">
            <a href="#" className="btn btn-outline-light text-danger sidebar-close">
                <i className="icon icon-close"></i>
            </a>
        </li>
      </ul>
  </header>
  
  <form className="px-20 px-xl-30">
  <div class="form-group">
                    
                    <select class="selectpicker" data-live-search="true" data-style="form-control py-6 border border-light" data-size="5" data-width="fill">
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-1.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Roelof Bekkenenks</span></span>">Roelof Bekkenenks</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-4.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Tony McDonald</span></span>">Tony McDonald</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-5.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Eunice Cross</span></span>">Eunice Cross</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-3.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Udom Paowsong</span></span>">Udom Paowsong</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-2.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Sofía Alcocer</span></span>">Sofía Alcocer</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-6.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Teng Jiang</span></span>">Teng Jiang</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-8.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Torsten Paulsson</span></span>">Torsten Paulsson</option>
                      <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-7.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Iruka Akuchi</span></span>">Iruka Akuchi</option>
                    </select>
                  </div>
    <div className="d-flex align-items-center">
      <div className="input-group-btn pr-8" data-toggle="buttons">
        <button className="btn btn-outline-light py-10" type="button" data-toggle="collapse" data-target="#discover-adv-filters">
          <i className="icon icon-16 icon-filters-alt text-primary"></i>
          <span className="sr-only">Advanced</span>
        </button>
      </div>
      <div className="input-control flex-one">
        <input type="text" className="form-control" placeholder="Search People" />
      </div>
    </div>
    
  </form>

  <div className="sidebar-body">
        
    <div className="collapse" id="discover-adv-filters">
      <form className="px-20 px-xl-30 pt-0">
        <div className="card card-unstyled mb-0">
          <div className="card-block px-0 pt-8 pb-0">

            <div className="form-group">
              <label>Join Date</label>
              <input type="text" className="form-control form-control-md datepicker" />
            </div>

            <div className="form-group">
              <label>Topics</label>
              <select className="selectpicker" 
                data-style="form-control py-12" multiple data-selected-text-format="count > 3" title="Select Topics" data-size="5" data-live-search="true">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
                <option>Onions</option>
                <option>Carots</option>
                <option>Apples</option>
                <option>Bananas</option>
                <option>Limes</option>
                <option>Vine</option>
                <option>Beer</option>
                <option>Water</option>
              </select>
            </div>

            <div className="form-group">
              <label>Expert</label>
              <select className="selectpicker show-tick" 
                data-style="form-control py-12" title="Select" 
                data-size="3" data-width="fill">
                <option>All</option>
                <option>Yes</option>
                <option>No</option>
              </select>
            </div>

            <div className="form-group">
              <label>Location</label>
              <select className="selectpicker show-tick" 
                data-style="form-control py-12" title="Select" 
                data-size="5" data-live-search="true">
                <option>Location 1</option>
                <option>Location 2</option>
                <option>Location 3</option>
                <option>Location 4</option>
                <option>Location 5</option>
                <option>Location 6</option>
                <option>Location 7</option>
                <option>Location 8</option>
                <option>Location 9</option>
                <option>Location 10</option>
                <option>Location 11</option>
              </select>
            </div>

          </div>
          <div className="card-footer d-flex align-items-center px-0">
              <span>
                <button className="btn btn-primary" type="button">Refine</button>
                <button className="btn btn btn-outline-light" type="button">Reset Filters</button>
              </span>
              <button className="btn btn btn-outline-light ml-auto" type="button"
                data-toggle="collapse" data-target="#discover-adv-filters">
                Cancel
              </button>
            </div>
        </div>
      </form>
    </div>

    <div style={{"display": "none"}}>
      <div className="card card-unstyled">
        <div className="card-block text-center pa-30 py-lg-40">
          <i className="icon icon-32 icon-notification text-muted mb-20"></i>
          <h6 className="mb-0">
            No results found for the selected filter.
          </h6>
        </div>
      </div>
    </div>

    <ul className="list-group list-group-flush list-group-chat">
                            
      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar avatar-gold">
                  <img src="/images/sample/user-basic-1.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Harrietta Souten</h5>
                  <p>Dental Hygienist</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar avatar-gold">
                  <span className="avatar-title bg-success rounded-circle">A</span>
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Aline McShee</h5>
                  <p>Marketing Assistant</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar avatar-gold">
                  <img src="/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Brietta Blogg</h5>
                  <p>Actuary</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar">
                  <img src="/images/sample/user-basic-3.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Karl Hubane</h5>
                  <p>Chemical Engineer</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar avatar-gold">
                  <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Jillana Tows</h5>
                  <p>Project Manager</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar">
                  <span className="avatar-title bg-info rounded-circle">AD</span>
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Alina Derington</h5>
                  <p>Nurse</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar">
                  <span className="avatar-title bg-warning rounded-circle">SK</span>
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Stevy Kermeen</h5>
                  <p>Associate Professor</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar avatar-gold">
                  <img src="/images/sample/user-basic-6.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Stevy Kermeen</h5>
                  <p>Senior Quality Engineer</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar">
                  <img src="/images/sample/user-basic-7.jpg" className="rounded-circle" alt="image" />
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Gloriane Shimmans</h5>
                  <p>Web Designer</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

      <li className="list-group-item" data-navigation-target="contact-information">
          <div>
              <figure className="avatar">
                  <span className="avatar-title bg-secondary rounded-circle">BP</span>
              </figure>
          </div>
          <div className="users-list-body">
              <div>
                  <h5>Bernhard Perrett</h5>
                  <p>Software Engineer</p>
              </div>
              <div className="users-list-action">
                  <div className="action-toggle">
                      <div className="dropdown">
                          <a data-toggle="dropdown" href="#">
                              <i className="icon icon-more"></i>
                          </a>
                          <div className="dropdown-menu dropdown-menu-right">
                              <a href="#" className="dropdown-item">New chat</a>
                              <a href="#" data-navigation-target="contact-information" className="dropdown-item">Profile</a>
                              <a href="#" className="dropdown-item">Add to contacts</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>

    </ul>

  </div>

</div>

       
       );
    
  }
}


export default Discover;
