import React, { Component } from "react";

import { favoritesStore } from "../../../stores";

class Favorites extends Component {

    constructor(props) {
        super(props);
        this.state =  {
          favoritesList :  favoritesStore.getState()
        }
      }     

  handleRemoveFavoritesClick(chat, event) {
    console.log("Remove Chat", chat)
  }   

  render() {
        let { favoritesList } = this.state
      return (
        <div id="favorites" className="sidebar">
  
          <header>
            <span className="flex-one flex-xlg-auto">
              Favorites
            </span>
            <ul className="list-inline">
              <li className="list-inline-item d-xlg-none d-inline">
                <a href="#" className="btn btn-outline-light text-danger sidebar-close">
                  <i className="icon icon-close"></i>
                </a>
              </li>
            </ul>
          </header>

          <form className="px-20 px-xl-30 pt-30 pt-xlg-20">
            <input type="text" className="form-control" placeholder="Search favorites" />
          </form>
  
          <div className="sidebar-body">
            
            
            {
                favoritesList.length > 0 
                ? <ul className="list-group list-group-flush users-list">
                
                 {
                    favoritesList.map((chat, index) => {
                        return <li className="list-group-item" key={`favorites-${index}`}>
                            <div>
                               <figure className={`avatar ${chat.hasUnread ? "avatar-gold" : ""}`}>
                                  {
                                    chat.image ? <img src={chat.image} className="rounded-circle" alt="image" /> : <span className="avatar-title bg-danger rounded-circle">{chat.name[0]}</span>
                                  }
                                </figure>
                            </div>
                            <div className="users-list-body">
                                <div>
                                     <h5>{chat.name}</h5>
                                      <p>{chat.lastMessgae}</p>
                                </div>
                                <div className="users-list-action">
                                    <div className="action-toggle">
                                        <div className="dropdown">
                                            <a data-toggle="dropdown" href="#">
                                                <i className="icon icon-more"></i>
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-right">
                                                <a href="#" className="dropdown-item">Open</a>
                                                <a href="#" className="dropdown-item" onClick={this.handleRemoveFavoritesClick.bind(this, chat)}>Remove favorites</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                    })
                 }
                

                {/*<li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Marvin Rohan</h5>
                            <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" className="dropdown-item">Remove favorites</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-3.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Frans Hanscombe</h5>
                            <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" className="dropdown-item">Remove favorites</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-4.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Karl Hubane</h5>
                            <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" className="dropdown-item">Remove favorites</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>*/}

            </ul>

            :   <div>
              <div className="card card-unstyled">
                <div className="card-block text-center pa-30 py-lg-40">
                  <i className="icon icon-32 icon-notification text-muted mb-20"></i>
                  <h6 className="mb-0">
                    No results found for the selected filter.
                  </h6>
                </div>
              </div>
            </div>
            }
            

          </div>
    </div>
       
       );
    
  }
}


export default Favorites;
