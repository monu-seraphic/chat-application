import React, { Component } from "react";
import { archiveStore } from "../../../stores";

class Archive extends Component {

  constructor(props) {
    super(props);
    this.state =  {
      archiveList :  archiveStore.getState()
    }
  }

  hndleRestoreClick(chat, event) {
    console.log("Restore Chat Object", chat)
  }

  render() {
      let { archiveList } = this.state
      return (
        <div id="archived" className="sidebar">
            <header>
              <span className="flex-one flex-xlg-auto">
                Archive
              </span>
              <ul className="list-inline">
                <li className="list-inline-item d-xlg-none d-inline">
                  <a href="#" className="btn btn-outline-light text-danger sidebar-close">
                    <i className="icon icon-close"></i>
                  </a>
                </li>
              </ul>
            </header>

            <form className="px-20 px-xl-30 pt-30 pt-xlg-20">
                <input type="text" className="form-control" placeholder="Search Archive" />
            </form>
  
            <div className="sidebar-body">
                  {
                    archiveList.length > 0 
                    ?
                    <ul className="list-group list-group-flush users-list">
                      {
                         archiveList.map((chat, index) => {
                          return <li className="list-group-item" key={`archive-${index}`}>
                              <div>
                                <figure className={`avatar ${chat.hasUnread ? "avatar-gold" : ""}`}>
                                  {
                                    chat.image ? <img src={chat.image} className="rounded-circle" alt="image" /> : <span className="avatar-title bg-danger rounded-circle">{chat.name[0]}</span>
                                  }
                                </figure>
                              </div>
                              <div className="users-list-body">
                                  <div>
                                      <h5>{chat.name}</h5>
                                      <p>{chat.lastMessgae}</p>
                                  </div>
                                  <div className="users-list-action">                      
                                      <div className="action-toggle">
                                          <div className="dropdown">
                                              <a data-toggle="dropdown" href="#">
                                                  <i className="icon icon-more"></i>
                                              </a>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a href="#" className="dropdown-item">Open</a>
                                                  <a href="#" className="dropdown-item" onClick={this.hndleRestoreClick.bind(this, chat)}>Restore</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a href="#" className="dropdown-item text-danger">Delete</a>
                                              </div>
                                          </div>
                                      </div>
                                      <small className="text-muted">{chat.lastMessgaeDate}</small>
                                  </div>
                              </div>
                          </li>
                        })
                      }
                      {/*<li className="list-group-item">
                          <div>
                            <figure className="avatar">
                                <span className="avatar-title bg-secondary rounded-circle">R</span>
                            </figure>
                          </div>
                          <div className="users-list-body">
                              <div>
                                  <h5>Rochelle Golley</h5>
                                  <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                              </div>
                              <div className="users-list-action">
                                  <div className="action-toggle">
                                      <div className="dropdown">
                                          <a data-toggle="dropdown" href="#">
                                              <i className="icon icon-more"></i>
                                          </a>
                                          <div className="dropdown-menu dropdown-menu-right">
                                              <a href="#" className="dropdown-item">Open</a>
                                              <a href="#" className="dropdown-item">Restore</a>
                                              <div className="dropdown-divider"></div>
                                              <a href="#" className="dropdown-item text-danger">Delete</a>
                                          </div>
                                      </div>
                                  </div>
                                  <small className="text-muted">02/01/2020</small>
                              </div>
                          </div>
                      </li>

                      <li className="list-group-item">
                          <div>
                              <figure className="avatar avatar-gold">
                                  <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
                              </figure>
                          </div>
                          <div className="users-list-body">
                              <div>
                                  <h5>Rochelle Golley</h5>
                                  <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                              </div>
                              <div className="users-list-action">
                                  <div className="action-toggle">
                                      <div className="dropdown">
                                          <a data-toggle="dropdown" href="#">
                                              <i className="icon icon-more"></i>
                                          </a>
                                          <div className="dropdown-menu dropdown-menu-right">
                                              <a href="#" className="dropdown-item">Open</a>
                                              <a href="#" className="dropdown-item">Restore</a>
                                              <div className="dropdown-divider"></div>
                                              <a href="#" className="dropdown-item text-danger">Delete</a>
                                          </div>
                                      </div>
                                  </div>
                                  <small className="text-muted">02/01/2020</small>
                              </div>
                          </div>
                      </li>*/}
                  </ul>
                    : <div >
                        <div className="card card-unstyled">
                          <div className="card-block text-center pa-30 py-lg-40">
                            <i className="icon icon-32 icon-notification text-muted mb-20"></i>
                            <h6 className="mb-0">
                              No results found for the selected filter.
                            </h6>
                          </div>
                        </div>
                      </div>
                  }
                  

                  
              </div>
            </div>
       
       );
    
  }
}


export default Archive;
