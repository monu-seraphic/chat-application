import React, { Component } from "react";
import { chatStore } from "../../../stores";
console.log("chatStore", chatStore)
class Chats extends Component {

  constructor(props) {
    super(props);
    this.state =  {
      chatList :  chatStore.getState()
    }
  }
  
  render() {
       let { chatList } = this.state; 
      return (
        <div id="chats" className="sidebar active">
          <header>
              <span className="flex-one flex-xlg-auto">
                Chats
              </span>
              
              <ul className="list-inline">

                <li className="list-inline-item" data-toggle="tooltip" title="New chat">
                    <a className="btn btn-outline-light" href="#" data-navigation-target="discover">
                      <i className="icon icon-plus text-primary"></i>
                    </a>
                </li>
                
                <li className="list-inline-item d-inline d-xlg-none ">
                    <a href="#" className="btn btn-outline-light text-danger sidebar-close">
                        <i className="icon icon-close"></i>
                    </a>
                </li>
              </ul>
          </header>
  
          <form className="px-20 px-xl-30 pb-0">
          <div class="form-group">
                    
                        <select class="selectpicker" data-live-search="true" data-style="form-control py-6 border border-light" data-size="5" data-width="fill">
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-1.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Roelof Bekkenenks</span></span>">Roelof Bekkenenks</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-4.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Tony McDonald</span></span>">Tony McDonald</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-5.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Eunice Cross</span></span>">Eunice Cross</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-3.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Udom Paowsong</span></span>">Udom Paowsong</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-2.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Sofía Alcocer</span></span>">Sofía Alcocer</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-6.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Teng Jiang</span></span>">Teng Jiang</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-8.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Torsten Paulsson</span></span>">Torsten Paulsson</option>
                          <option data-content="<span class='d-flex align-items-center min-w-0 max-fw-160'><figure class='avatar fw-20 fh-20 mr-8'><img src='//static1.askaway.com/images/sample/user-basic-7.jpg' class='rounded-circle' alt='image'></figure><span class='d-block py-2 text-truncate'>Iruka Akuchi</span></span>">Iruka Akuchi</option>
                        </select>
                      </div>
            <div className="d-flex align-items-center mb-12">
              <div className="input-group-btn pr-8" data-toggle="buttons">
                <button className="btn btn-outline-light py-10" type="button" data-toggle="collapse" data-target="#chats-adv-filters">
                  <i className="icon icon-16 icon-filters-alt text-primary"></i>
                  <span className="sr-only">Advanced</span>
                </button>
              </div>
              <div className="input-control flex-one">
                <input type="text" className="form-control" placeholder="Search chats" />
              </div>
            </div>

            <div className="scrollable-container mx-n-30 px-30 mb-12">
              <div className="scrollable-x">
                <ul className="list-inline flex-nowrap">

                  <li className="list-inline-item ml-0 mr-8 d-flex flex-column align-items-start">
                    <label>Membership</label>
                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                      <label className="btn btn-outline-light active">
                        <input type="radio" name="quick-toggles-1" id="quick-toggle-1-1" autoComplete="off" checked />
                        All
                      </label>
                      <label className="btn btn-outline-light">
                        <input type="radio" name="quick-toggles-1" id="quick-toggle-1-2" autoComplete="off" />
                        Has Stars
                      </label>
                      <label className="btn btn-outline-light">
                        <input type="radio" name="quick-toggles-1" id="quick-toggle-1-3" autoComplete="off" />
                        Gold Member
                      </label>
                    </div>
                  </li>

                  <li className="list-inline-item ml-0 mr-8 d-flex flex-column align-items-start">
                    <label>Join Date</label>
                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                      <label className="btn btn-outline-light active">
                        <input type="radio" name="quick-toggles-2" id="quick-toggle-2-1" autoComplete="off" checked />
                        All
                      </label>
                      <label className="btn btn-outline-light">
                        <input type="radio" name="quick-toggles-2" id="quick-toggle-2-2" autoComplete="off" />
                        Today
                      </label>
                      <label className="btn btn-outline-light">
                        <input type="radio" name="quick-toggles-2" id="quick-toggle-2-3" autoComplete="off" />
                        This Week
                      </label>
                      <label className="btn btn-outline-light">
                        <input type="radio" name="quick-toggles-2" id="quick-toggle-2-4" autoComplete="off" />
                        Last Week
                      </label>
                    </div>
                  </li>

                </ul>
              </div>
            </div>
            
          </form>

          <div className="sidebar-body">
            <div className="collapse" id="chats-adv-filters">
              <form className="px-20 px-xl-30 pt-0">
                <div className="card card-unstyled mb-0">
                  <div className="card-block px-0 pt-8 pb-0">
                    <div className="form-group">
                      <label>Join Date</label>
                      <input type="text" className="form-control form-control-md datepicker" />
                    </div>

                    <div className="form-group">
                      <label>Topics</label>
                      <select className="selectpicker" 
                        data-style="form-control py-12" multiple data-selected-text-format="count > 3" title="Select Topics" data-size="5" data-live-search="true">
                        <option>Mustard</option>
                        <option>Ketchup</option>
                        <option>Relish</option>
                        <option>Onions</option>
                        <option>Carots</option>
                        <option>Apples</option>
                        <option>Bananas</option>
                        <option>Limes</option>
                        <option>Vine</option>
                        <option>Beer</option>
                        <option>Water</option>
                      </select>
                    </div>
                  </div>
                  <div className="card-footer d-flex align-items-center px-0">
                      <span>
                        <button className="btn btn-primary" type="button">Refine</button>
                        <button className="btn btn btn-outline-light" type="button">Reset Filters</button>
                      </span>
                      <button className="btn btn btn-outline-light ml-auto" type="button"
                        data-toggle="collapse" data-target="#chats-adv-filters">
                        Cancel
                      </button>
                    </div>
                </div>
              </form>
            </div>

            {
                !(chatList.length > 0) ? <div style={{"display": "none"}}>
                  <div className="card card-unstyled">
                    <div className="card-block text-center pa-30 py-lg-40">
                      <i className="icon icon-32 icon-notification text-muted mb-20"></i>
                      <h6 className="mb-0">
                        No results found for the selected filter.
                      </h6>
                    </div>
                  </div>
                </div>
                : <ul className="list-group list-group-flush">
                    {
                        chatList.map((chat, index) => {
                            return <li className={`list-group-item ${chat.unreadCount ? "updated-chat": ""}`} key={`chat-${index}`}>
                                <figure className={`avatar ${chat.hasUnread ? "avatar-gold" : ""}`}>
                                  {
                                    chat.image ? <img src={chat.image} className="rounded-circle" alt="image" /> : <span className="avatar-title bg-danger rounded-circle">{chat.name[0]}</span>
                                  }
                                </figure>
                                <div className="users-list-body">
                                    <div>
                                        <h5>{chat.name}</h5>
                                        {
                                           chat.lastMessgaeType === "Text" ? <p>What's up, how are you?</p> : null
                                        }
                                        {
                                           chat.lastMessgaeType === "Image" ? <p><i class="icon icon-camera-fill mr-1"></i> Photo</p> : null
                                        }
                                        {
                                           chat.lastMessgaeType === "Video" ? <p><i class="icon icon-videos mr-1"></i>Video</p> : null
                                        }
                                        
                                    </div>
                                    <div className="users-list-action">
                                        { chat.unreadCount ? <div className="new-message-count">{chat.unreadCount}</div>: null }
                                        <small className={chat.unreadCount? "text-primary": ""}>{chat.lastMessgaeDate}</small>
                                    </div>
                                </div>
                            </li>
                        })
                    }
                
                
                {/*<li className="list-group-item updated-chat">
                    <figure className="avatar">
                        <img src="/images/sample/user-basic-2.jpg" className="rounded-circle" alt="image" />
                    </figure>
                    <div className="users-list-body">
                        <div>
                            <h5 className="text-primary">Forest Kroch</h5>
                            <p>
                                <i className="icon icon-camera-fill mr-1"></i> Photo
                            </p>
                        </div>
                        <div className="users-list-action">
                            <div className="new-message-count">1</div>
                            <small className="text-primary">Yesterday</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item open-chat">
                    <div>
                        <figure className="avatar">
                            <img src="/images/sample/user-large-8.png" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Byrom Guittet</h5>
                            <p>I sent you all the files. Good luck with 😃</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">11:05 AM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item updated-chat">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-4.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Margaretta Worvell</h5>
                            <p>I need you today. Can you come with me?</p>
                        </div>
                        <div className="users-list-action">
                            <div className="new-message-count">88</div>
                            <small className="text-primary">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <figure className="avatar">
                        <span className="avatar-title rounded-circle">
                          <img src="/images/user-guest-avatar-neutral.svg" className="rounded-circle" alt="image" />
                        </span>
                    </figure>
                    <div className="users-list-body">
                        <div>
                            <h5>😍 My Family 😍</h5>
                            <p>Hello!!!</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">Yesterday</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar">
                            <span className="avatar-title rounded-circle">
                              <img src="/images/user-guest-avatar-neutral.svg" className="rounded-circle" alt="image" />
                            </span>
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Jennica Kindred</h5>
                            <p>
                                <i className="icon icon-videos mr-1"></i>
                                Video
                            </p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <span className="avatar-title bg-info rounded-circle">M</span>
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Marvin Rohan</h5>
                            <p>Have you prepared the files?</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">Yesterday</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-5.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Townsend Seary</h5>
                            <p>Where are you?</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <span className="avatar-title bg-secondary rounded-circle">G</span>
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Gibb Ivanchin</h5>
                            <p>I want to visit them.</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-6.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Harald Kowalski</h5>
                            <p>Reports are ready.</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar">
                            <span className="avatar-title bg-success rounded-circle">A</span>
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Afton McGilvra</h5>
                            <p>I do not know where is it. Don't ask me, please.</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>

                <li className="list-group-item">
                    <div>
                        <figure className="avatar avatar-gold">
                            <img src="/images/sample/user-basic-7.jpg" className="rounded-circle" alt="image" />
                        </figure>
                    </div>
                    <div className="users-list-body">
                        <div>
                            <h5>Alexandr Donnelly</h5>
                            <p>Can anyone enter the meeting?</p>
                        </div>
                        <div className="users-list-action">
                            <div className="action-toggle">
                                <div className="dropdown">
                                    <a data-toggle="dropdown" href="#">
                                        <i className="icon icon-more"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item">Open</a>
                                        <a href="#" data-navigation-target="contact-information"
                                            className="dropdown-item">Profile</a>
                                        <a href="#" className="dropdown-item">Add to archive</a>
                                        <div className="dropdown-divider"></div>
                                        <a href="#" className="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <small className="text-muted">03:41 PM</small>
                        </div>
                    </div>
                </li>*/}
            </ul>

                
            }

            

            

          </div>

        </div>

       
       );
    
  }
}


export default Chats;
