import React from 'react';
import "./css/bundle.css";
import "./css/app.css";
import "./css/main-aa-base.css";

import LayoutNavigation from "./components/Layout/LayoutNavigation";

import Archive from "./components/Layout/Sidebar/Archive";
import Chats from "./components/Layout/Sidebar/Chats";
import Contacts from "./components/Layout/Sidebar/Contacts";
import Discover from "./components/Layout/Sidebar/Discover";
import Favorites from "./components/Layout/Sidebar/Favorites";
import ChatBody from "./components/Chat/Body";
import ChatFooter from "./components/Chat/Footer";
import ChatHeader from "./components/Chat/Header";
import MyProfile from "./components/User/MyProfile";
import ContactInfo from "./components/User/ContactInfo";
import Star from "./components/User/BuyStar";
import EditProfile from "./components/User/EditProfile";
import Account from "./components/Layout/Sidebar/Accounts";
import UnlockSingle from "./components/User/UnlockSingle";
import UnlockGroup from "./components/User/UnlockGroup";
import AddContacts from "./components/User/AddContacts";
import AddAccounts from "./components/User/AddAccount";



function App() {
  return (
    <div>
          <main className="layout">
          <nav className="navigation">
            <LayoutNavigation />
          </nav>
          <div className="content">
            <section className="sidebar-group">
                <Chats />
                <Discover />
                <Contacts />
                <Favorites />
                <Archive />
             <Account/>
            </section>
            <section class="chat">
              <ChatHeader />
               
              
                
              <ChatBody />
              <ChatFooter />
            </section>
            <section class="sidebar-group">
              <ContactInfo />
              <MyProfile />
              {/* <EditProfile/> */}
            
            </section>
        </div>
   
    </main>
   
    <Star/>
        <EditProfile/>
        <UnlockSingle></UnlockSingle>
       
        <UnlockGroup></UnlockGroup>
        <AddContacts></AddContacts>
        <AddAccounts></AddAccounts>
</div>
  );
}

export default App;
